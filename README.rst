pymodaq_plugins_thg (THG project)
##########################################


Set of PyMoDAQ plugins for THG project


Authors
=======

* Benedikt Burger


Instruments
===========

Below is the list of instruments included in this plugin

Actuators
+++++++++

* **LECO_Trinamic**: Control of Trinamic motion control stepper motor cards

Viewer0D
++++++++

* **0DViewer_LECO_LISTENER**: Get data via a listener (pymodaq_plugins_leco)
* **xxx**: control of xxx 0D detector

Viewer1D
++++++++

* **yyy**: control of yyy 1D detector
* **xxx**: control of xxx 1D detector


Viewer2D
++++++++

* **Basler**: control of Basler cameras


PID Models
==========


Extensions
==========


Infos
=====

