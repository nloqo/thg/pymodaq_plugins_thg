# from pymodaq_plugins_pylablib_camera.daq_viewer_plugins.plugins_2D.daq_2Dviewer_GenericPylablibCamera import DAQ_2DViewer_GenericPylablibCamera
# see https://github.com/rgeneaux/pymodaq_plugins_test_pylablib
from pymodaq.utils.parameter import Parameter
from pymodaq_plugins_thg.daq_viewer_plugins.plugins_2D.daq_2Dviewer_GenericPylablibCamera import DAQ_2DViewer_GenericPylablibCamera, ThreadCommand
from pymodaq.control_modules.viewer_utility_classes import main

from pylablib.devices import Basler


class DAQ_2DViewer_Basler(DAQ_2DViewer_GenericPylablibCamera):
    """
    """
    # Generate a  **list**  of available cameras.
    # Two cases:
    # 1) Some pylablib classes have a .list_cameras method, which returns a list of available cameras, so we can just use that
    # 2) Other classes have a .get_cameras_number(), which returns the number of connected cameras
    #    in this case we can define the list as self.camera_list = [*range(number_of_cameras)]

    # For Basler, this returns a list of friendly names
    camera_list = [cam.friendly_name for cam in Basler.list_cameras()]

    # Update the params (nothing to change here)
    params = DAQ_2DViewer_GenericPylablibCamera.params
    params[next((i for i, item in enumerate(params) if item["name"] == "camera_list"), None)]['limits'] = camera_list

    def init_controller(self):
        # Define the camera controller.
        # Use any argument necessary (serial_number, camera index, etc.) depending on the camera

        # Init camera with currently selected friendly name
        friendly_name = self.settings["camera_list"]
        self.emit_status(ThreadCommand('Update_Status', [f"Trying to connect to {friendly_name}", 'log']))
        camera_list = Basler.list_cameras()
        for cam in camera_list:
            if cam.friendly_name == friendly_name:
                name = cam.name
                return Basler.BaslerPylonCamera(name=name)
        self.emit_status(ThreadCommand('Update_Status', ["Camera not found", 'log']))
        raise ValueError(f"Camera with name {friendly_name} not found anymore.")


if __name__ == '__main__':
    main(__file__)
